<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Spring Boot Form Handling Example</title>
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Sofia">
   <style><%@include file="/WEB-INF/jsp/style/style.css"%></style>

</head>
<body>
    <h1>Hello!</h1>
    <p>
    <a href="/person/personAdd">Registration</a>
    <a href="/person/allPersonShow">Show all users</a>
    <a href="/card/cardAdd">Add card</a>
    <a href="/card/allCardsShow">Show all cards</a>
    <a href="/product/productAdd">Add product</a>
    <a href="/product/allProductShow">Show all product</a>
</p>


</body>
</html>