package com.example.demo.controller;

import com.example.demo.dtos.PersonDto;
import com.example.demo.model.Person;
import com.example.demo.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(path = "/person")
public class PersonController {

    @Autowired
    private PersonService personService;

    @PostMapping("/personAdd")
    public String submitFormPersonAdd(@ModelAttribute("person") PersonDto personDto) {
        Person person = new Person();
        person.setId(personDto.getId());
        person.setFirstName(personDto.getFirstName());
        person.setLastName(personDto.getLastName());
        person.setEmail(personDto.getEmail());
        personService.createPerson(person);
        return "personAddSucces";
    }

    @RequestMapping(value = "/personAdd", method = RequestMethod.GET)
    public String showFormPersonAdd(Model model) {
        PersonDto person = new PersonDto();
        model.addAttribute("person", person);
        return "personAdd";
    }

    @GetMapping("/allPersonShow")
    public String showFormPersonFind(Model model) {
        List<Person> personList = personService.getAllPersons();
        List<PersonDto> personDtoList = new ArrayList<>();
        PersonDto personDto;
        for (Person p : personList) {
            personDto = new PersonDto();
            personDto.setId(p.getId());
            personDto.setFirstName(p.getFirstName());
            personDto.setLastName(p.getLastName());
            personDto.setEmail(p.getEmail());
            personDtoList.add(personDto);
        }
        model.addAttribute("persons", personDtoList);
        return "personList";
    }

}
