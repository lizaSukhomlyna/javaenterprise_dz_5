package com.example.demo.controller;


import com.example.demo.dtos.ProducDto;
import com.example.demo.model.Card;
import com.example.demo.model.Product;
import com.example.demo.service.CardService;
import com.example.demo.service.PersonService;
import com.example.demo.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping(path = "/product")
public class ProductController {

    @Autowired
    private ProductService productService;
    @Autowired
    private CardService cardService;

    @Autowired
    private PersonService personService;


    @PostMapping("/productAdd")
    public String submitFormProductAdd(@ModelAttribute("product") ProducDto productDto) {
        Product product = new Product();
        product.setId(productDto.getId());
        product.setPersonId(cardService.getCardById(productDto.getCard_id()).getIdPerson());
        product.setCard_id(productDto.getCard_id());
        product.setName(productDto.getName());
        product.setCost(productDto.getCost());
        productService.addProductByPersonId(product);
        cardService.calculateCostOfCardById(product.getCard_id());
        return "productAddSucces";
    }

    @GetMapping("/productAdd")
    public String showFormProductAdd(Model model) {
        ProducDto productDto = new ProducDto();
        List<Long> cardIdList = cardService.getAllCards()
                .stream()
                .map(Card::getIdCard)
                .collect(Collectors.toList());

        model.addAttribute("product", productDto);
        model.addAttribute("cardIdList", cardIdList);
        return "productsAdd";
    }

    @GetMapping("/allProductShow")
    public String showFormProductFind(Model model) {

        List<Product> products = productService.getProductsByPersonId(1L);
        List<ProducDto> productDtos = new ArrayList<>();
        ProducDto productDto;
        for (Product pr : products) {
            productDto = new ProducDto();
            productDto.setId(pr.getId());
            productDto.setName(pr.getName());
            productDto.setCost(pr.getCost());
            productDto.setCard_id(pr.getCard_id());
            productDtos.add(productDto);
        }
        model.addAttribute("products", productDtos);
        return "productsList";
    }

    @PostMapping("/add")
    public String addProductByPersonId(@RequestBody Product product) {
        return productService.addProductByPersonId(product);
    }

    @GetMapping("/getProductsById")
    public String getProductsById(@RequestParam Long id) {
        return productService.getProductsByPersonIdInString(id);
    }
}
