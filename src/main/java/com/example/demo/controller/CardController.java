package com.example.demo.controller;

import com.example.demo.dtos.CardDto;
import com.example.demo.model.Card;
import com.example.demo.model.Person;
import com.example.demo.repository.CardRepository;
import com.example.demo.repository.PersonRepository;
import com.example.demo.service.CardService;
import com.example.demo.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping(path = "/card")
public class CardController {

    @Autowired
    private CardService cardService;

    @Autowired
    private PersonService personService;

    @Autowired
    private CardRepository cardRepository;

    @Autowired
    PersonRepository personRepository;

    @PostMapping("/cardAdd")
    public String submitFormCardAdd(@ModelAttribute("card") CardDto cardDto) {
        Card card = new Card();
        card.setIdCard(cardDto.getIdCard());
        card.setIdPerson(cardDto.getIdPerson());
        card.setCost(BigDecimal.ZERO);
        card.setProducts(cardDto.getProducts());
        cardService.createCardByPersonId(card);
        return "cardAddSucces";
    }

    @RequestMapping(value = "/cardAdd", method = RequestMethod.GET)
    public String showFormCardAdd(Model model) {
        CardDto card = new CardDto();
        List<Long> personIdList = personService.getAllPersons()
                .stream()
                .map(Person::getId)
                .collect(Collectors.toList());

        model.addAttribute("personIdList", personIdList);
        model.addAttribute("card", card);
        return "cardAdd";
    }

    @GetMapping("/allCardsShow")
    public String showFormCardFind(Model model) {
        List<Card> cards = cardService.getAllCards();
        List<CardDto> cardsDto = new ArrayList<>();
        for (Card card :
                cards) {
            CardDto cardDto = new CardDto();
            cardDto.setIdCard(card.getIdCard());
            cardDto.setIdPerson(card.getIdPerson());
            cardDto.setProducts(card.getProducts());
            cardDto.setCost(card.getCost());
            cardsDto.add(cardDto);
        }
        model.addAttribute("cards", cardsDto);
        return "cardsList";
    }

    @PostMapping("/getCost")
    public String submitFormCardAdd(Model model) {
        Long cardId = (Long) model.getAttribute("idCard");
        cardService.calculateCostOfCardById(cardId);
        return "productsCostShow";
    }

    @RequestMapping(value = "/getCost", method = RequestMethod.GET)
    public String showFormCostFind(Model model) {
        List<Long> cardIdList = new ArrayList<>();
        cardRepository.findAll().forEach(el -> cardIdList.add(el.getIdCard()));
        List<Card> cards = cardService.getAllCards();
        List<CardDto> cardsDto = new ArrayList<>();
        for (Card card :
                cards) {
            CardDto cardDto = new CardDto();
            cardDto.setIdCard(card.getIdCard());
            cardDto.setIdPerson(card.getIdPerson());
            cardDto.setProducts(card.getProducts());
            cardDto.setCost(card.getCost());
            cardsDto.add(cardDto);
        }
        model.addAttribute("cards", cardsDto);
        model.addAttribute("cardIdList", cardIdList);
        return "productsCost";
    }


}
