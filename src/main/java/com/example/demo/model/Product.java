package com.example.demo.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.math.BigDecimal;

@Getter
@Setter
@Entity
@Table(name = "products")
@ToString
public class Product {

    @Id
    @Column(name = "product_id")
    private Long id;

    @Column(name = "personId")
    private Long personId;

    @Column(name = "card_id")
    private Long card_id;

    private String name;
    private BigDecimal cost;


    public BigDecimal getCost() {
        return cost;
    }


}
