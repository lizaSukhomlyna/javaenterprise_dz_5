package com.example.demo.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "cards")
@ToString(exclude = "person")
public class Card {

    @Id
    @Column(name = "card_id")

    private Long idCard;

    @Column(name = "person_id")
    private Long idPerson;


    @Column(name = "cost")
    private BigDecimal cost;

    @Column(name = "products")
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "card_id")
    List<Product> products = new ArrayList<>();


}
