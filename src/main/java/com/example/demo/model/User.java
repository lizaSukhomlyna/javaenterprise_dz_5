package com.example.demo.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.sql.Date;

@Getter
@Setter
@ToString
public class User {
    private String name;
    private String email;
    private String gender;
    private String password;
    private String profession;
    private String note;
    private Date birthday;
    private boolean married;


}