package com.example.demo.service;


import com.example.demo.model.Person;

import java.util.List;

public interface PersonService {
    String createPerson(Person person);
    List<Person> getAllPersons();

}
