package com.example.demo.service;


import com.example.demo.model.Product;

import java.util.List;

public interface ProductService {

    String addProductByPersonId(Product product);

    String getProductsByPersonIdInString(Long id);

    List<Product> getProductsByPersonId(Long id);


}
