package com.example.demo.service;

import com.example.demo.model.Product;
import com.example.demo.repository.PersonRepository;
import com.example.demo.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private PersonRepository personRepository;

    @Override
    public String addProductByPersonId(Product product) {
        try {
            productRepository.save(product);
            return "Product " + product.getName() + " with id:" + product.getId() + " was adding";
        } catch (Exception e) {
            e.printStackTrace();
            return e.getMessage();
        }
    }

    @Override
    public String getProductsByPersonIdInString(Long id) {
        try {
            return productRepository
                    .findById(id)
                    .orElseThrow(() -> new Exception("Product wasn't found" + new Product())).toString();
        } catch (Exception e) {
            return e.getMessage();
        }
    }

    @Override
    public List<Product> getProductsByPersonId(Long id) {
        try {
            List<Product> products = new ArrayList<>();
            personRepository.findById(id).orElseThrow(() -> new Exception("Person wasn't found" + new Product()))
                    .getCards().forEach(card -> products.addAll(card.getProducts()));
            return products;
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }
}
