package com.example.demo.dtos;

import lombok.Data;

@Data
public class PersonDto {

    private Long id;
    private String firstName;
    private String lastName;
    private String email;

}
